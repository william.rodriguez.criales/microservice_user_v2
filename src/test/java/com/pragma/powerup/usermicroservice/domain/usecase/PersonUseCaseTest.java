package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.usermicroservice.domain.model.Person;
import com.pragma.powerup.usermicroservice.domain.spi.IPersonPersistencePort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PersonUseCaseTest {

    @Mock
    IPersonPersistencePort iPersonPersistencePort;
    @InjectMocks
    PersonUseCase personUseCase;


    @ParameterizedTest
    @MethodSource("invalidRequest")
    void savePerson(Person person, String message) {
        //arrange

        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> personUseCase.savePerson(person));
        //assert
        assertEquals(message, validationException.getMessage());
        verify(iPersonPersistencePort, never()).savePerson(person);
    }

    static Stream<Arguments> invalidRequest() {
        return Stream.of(Arguments.of(new Person(1L, "", "", "", "", "", "", "", "", "", null),
                        "empty name"),
                Arguments.of(new Person(1L, "name", "", "", "", "", "", "", "", "", null),
                        "empty surname"),
                Arguments.of(new Person(1L, "name", "surname", "", "", "", "", "", "", "", null),
                        "empty email"),
                Arguments.of(new Person(1L, "name", "surname", "email", "", "", "", "", "", "", null),
                        "invalid email"),
                Arguments.of(new Person(1L, "name", "surname", "email@gmail.com", "", "", "", "", "", "", null),
                        "empty phone"),
                Arguments.of(new Person(1L, "name", "surname", "email@gmail.com", "111", "", "", "", "", "", null),
                        "invalid phone"),
                Arguments.of(new Person(1L, "name", "surname", "email@gmail.com", "+5711111111", "", "", "", "", "", null),
                        "empty Dni number"),
                Arguments.of(new Person(1L, "name", "surname", "email@gmail.com", "+5711111111", "", "", "dni", "", "", null),
                        "invalid Dni Number"),
                Arguments.of(new Person(1L, "name", "surname", "email@gmail.com", "+5711111111", "", "", "111", "", "", null),
                        "empty password"),
                Arguments.of(new Person(1L, "name", "surname", "email@gmail.com", "+5711111111", "", "", "111", "", "111", null),
                        "empty birthdate"),
                Arguments.of(new Person(1L, "name", "surname", "email@gmail.com", "+5711111111", "", "", "111", "", "111", LocalDate.now().minusYears(17)),
                        "the person is lower than 18 years old"),
                Arguments.of(new Person(1L, "name", "surname", "email@gmail.com", "+5711111111", "", "", "111", "", "111", LocalDate.now().minusYears(18).plusMonths(1)),
                        "the person is lower than 18 years old"),
                Arguments.of(new Person(1L, "name", "surname", "email@gmail.com", "+5711111111", "", "", "111", "", "111", LocalDate.now().minusYears(18).plusDays(1)),
                        "the person is lower than 18 years old")


        );

    }

    @Test
    void savePersonSuccessfully() {
        //arrange
        Person person = new Person(1L, "name", "surname", "wmail@gmail.com", "+571111111", "cll 9-1-1", "cc", "111111", "1", "1234", LocalDate.of(1992, 3, 3));
        //when
        when(iPersonPersistencePort.savePerson(any(Person.class))).thenReturn(person);
        //act
        personUseCase.savePerson(person);
        //assert
        verify(iPersonPersistencePort, times(1)).savePerson(person);
    }
}
