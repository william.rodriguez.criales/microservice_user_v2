package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PersonRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PersonResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPersonHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPersonRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPersonResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.IPersonServicePort;
import com.pragma.powerup.usermicroservice.domain.model.Person;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonHandlerImpl implements IPersonHandler {
    private final IPersonServicePort personServicePort;
    private final IPersonRequestMapper personRequestMapper;
    private final IPersonResponseMapper personResponseMapper;

    @Override
    public PersonResponseDto savePerson(PersonRequestDto personRequestDto) {
        Person person = personServicePort.savePerson(personRequestMapper.toPerson(personRequestDto));
        return personResponseMapper.toPersonResponseDto(person);

    }
}
