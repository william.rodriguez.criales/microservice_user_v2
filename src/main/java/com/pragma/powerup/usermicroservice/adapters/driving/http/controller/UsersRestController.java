package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PersonRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserRoleResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserRoleHandler;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UsersRestController {
    private final IUserRoleHandler userRoleHandler;
    @Operation(summary = "Add a new Owner",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Owner created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "Person already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/owner")
    public ResponseEntity<UserRoleResponseDto> saveOwner(@RequestBody PersonRequestDto personRequestDto) {
        UserRoleResponseDto userRoleResponseDto = userRoleHandler.saveUser(personRequestDto, Constants.OWNER_ROLE_ID);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(userRoleResponseDto);
    }
}
