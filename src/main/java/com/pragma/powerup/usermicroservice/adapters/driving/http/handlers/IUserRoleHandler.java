package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PersonRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserRoleResponseDto;

public interface IUserRoleHandler {
    UserRoleResponseDto saveUser(PersonRequestDto personRequestDto, Long idRole);
}
