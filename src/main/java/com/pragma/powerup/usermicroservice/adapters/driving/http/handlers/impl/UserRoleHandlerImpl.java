package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PersonRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PersonResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserRoleResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPersonHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserRoleHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IUserRoleResponseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserRoleHandlerImpl implements IUserRoleHandler {

    private final IPersonHandler personHandler;
    private final IUserHandler userHandler;
    private final IUserRoleResponseMapper userRoleResponseMapper;



    @Override
    public UserRoleResponseDto saveUser(PersonRequestDto personRequestDto, Long idRole) {
        PersonResponseDto personResponseDto = personHandler.savePerson(personRequestDto);
        userHandler.saveUser(new UserRequestDto(personResponseDto.getId(), idRole));
        return userRoleResponseMapper.toUserRoleResponseDto(personResponseDto, idRole);

    }
}
