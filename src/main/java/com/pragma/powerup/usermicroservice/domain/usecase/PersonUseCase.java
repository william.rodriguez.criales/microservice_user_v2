package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.usermicroservice.domain.model.Person;
import com.pragma.powerup.usermicroservice.domain.spi.IPersonPersistencePort;
import com.pragma.powerup.usermicroservice.domain.api.IPersonServicePort;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.Period;

public class PersonUseCase implements IPersonServicePort {
    private final IPersonPersistencePort personPersistencePort;

    public PersonUseCase(IPersonPersistencePort personPersistencePort) {
        this.personPersistencePort = personPersistencePort;
    }

    @Override
    public Person savePerson(Person person) {

        if (isEmpty(person.getName())) {
            throw new ValidationException("empty name");
        }

        if (isEmpty(person.getSurname())) {
            throw new ValidationException("empty surname");
        }

        if (isEmpty(person.getMail())) {
            throw new ValidationException("empty email");
        } else if (!isEmailValid(person.getMail())) {
            throw new ValidationException("invalid email");
        }

        if (isEmpty(person.getPhone())) {
            throw new ValidationException("empty phone");
        } else if (!isPhoneValid(person.getPhone())) {
            throw new ValidationException("invalid phone");
        }

        if (isEmpty(person.getDniNumber())) {
            throw new ValidationException("empty Dni number");
        } else if (!isDniNumberValid(person.getDniNumber())) {
            throw new ValidationException(("invalid Dni Number"));
        }

        if (isEmpty(person.getPassword())) {
            throw new ValidationException("empty password");
        }

        if (isDateEmpty(person.getBirthdate())) {
            throw new ValidationException("empty birthdate");
        } else if (!isAgeGreaterThan18(person.getBirthdate())) {
            throw new ValidationException(("the person is lower than 18 years old"));
        }

        return personPersistencePort.savePerson(person);

    }

    //validacion email
    private boolean isEmailValid(String email) {
        return email.matches("^[^@]+@[^@]+\\.[a-zA-Z]{2,}$");
    }


    //validacion phone: numeros menores que 13 digitos y mayores a 7 digitos, debe permiter el +
    private boolean isPhoneValid(String phone) {
        return phone.matches("^[+]?(\\d){7,12}$");

    }

    private boolean isDniNumberValid(String dniNumber) {
        return dniNumber.matches("^(\\d)*$");
    }

    private boolean isAgeGreaterThan18(LocalDate birthDate) {
        LocalDate now = LocalDate.now();
        return Period.between(birthDate, now).getYears()>18;
    }

    private boolean isEmpty(String field) {
        //return field.matches("^[''' ']*$") || field==null ; opcion 1
        return StringUtils.isEmpty(field);
    }

    private boolean isDateEmpty(LocalDate field) {
        return ObjectUtils.isEmpty(field);
    }


}
