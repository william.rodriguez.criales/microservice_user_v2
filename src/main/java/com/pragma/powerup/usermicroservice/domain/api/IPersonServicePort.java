package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.domain.model.Person;

public interface IPersonServicePort {
    Person savePerson(Person person);
}
