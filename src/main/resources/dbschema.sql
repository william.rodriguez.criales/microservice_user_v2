DROP DATABASE IF EXISTS `powerup`;
CREATE DATABASE  IF NOT EXISTS `powerup`;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `powerup`.`person`;
CREATE TABLE `powerup`.`person` (
  `id` bigint(20) NOT NULL,
  `address` varchar(45) DEFAULT NULL,
  `dni_number` varchar(45) NOT NULL,
  `id_dni_type` varchar(20) DEFAULT NULL,
  `id_person_type` varchar(20) DEFAULT NULL,
  `mail` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `password` varchar(70) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `token_password` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- autoincrementar id en base de datos
ALTER TABLE `powerup`.`person`
CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT ;


--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `powerup`.`role`;
CREATE TABLE `powerup`.`role` (
  `id` bigint(20) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
--
-- Dumping data for table `role`
--

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `powerup`.`user`;
CREATE TABLE `powerup`.`user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_person` bigint(20) DEFAULT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_person_idx` (`id_person`),
  KEY `fk_role_idx` (`id_role`),
  CONSTRAINT `fk_person` FOREIGN KEY (`id_person`) REFERENCES `person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);


-- añadir columna
alter table `powerup`.`person` add column `birthdate` date NOT NULL;

-- modificacion tipo de datos
ALTER TABLE `powerup`.`person`
CHANGE COLUMN `id_dni_type` `id_dni_type` VARCHAR(10) NULL DEFAULT NULL ,
CHANGE COLUMN `id_person_type` `id_person_type` VARCHAR(10) NULL DEFAULT NULL ;


